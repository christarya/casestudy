package com.hydrane;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

public class Affinity_Power {

  private static final boolean ElemEmpty = true;
  public WebDriver driver;
  JavascriptExecutor jse;

  public void correct() {
    try {
      // fire - meteor
      driver.findElement(By.id("name")).sendKeys("Kristine Mukuchyan");
      driver.findElement(By.id("birthDate")).sendKeys("11/24/1991");
      jse = (JavascriptExecutor) driver;
      jse.executeScript("scroll(0, 800)");
      driver.findElement(By.id("affinity")).sendKeys("fire");
      driver.findElement(By.id("power")).sendKeys("meteor");
      driver.findElement(By.name("selfish")).click();
      driver.findElement(By.xpath("//button[contains(text(), 'Submit')]")).click();
      System.out.println("fire - meteor created");
      Thread.sleep(4000);
      driver.navigate().back();
      // fire - spear
      driver.findElement(By.id("name")).sendKeys("Karen Kalpakchyan");
      driver.findElement(By.id("birthDate")).sendKeys("08/30/1989");
      jse = (JavascriptExecutor) driver;
      jse.executeScript("scroll(0, 800)");
      driver.findElement(By.id("affinity")).sendKeys("fire");
      driver.findElement(By.id("power")).sendKeys("Fire Spear");
      driver.findElement(By.name("selfish")).click();
      driver.findElement(By.xpath("//button[contains(text(), 'Submit')]")).click();
      System.out.println("fire - spear created");
      Thread.sleep(4000);
      driver.navigate().back();      
      // water - rain
      driver.findElement(By.id("name")).sendKeys("Jack Williams");
      driver.findElement(By.id("birthDate")).sendKeys("10/30/1960");
      jse = (JavascriptExecutor) driver;
      jse.executeScript("scroll(0, 800)");
      driver.findElement(By.id("affinity")).sendKeys("water");
      driver.findElement(By.id("power")).sendKeys("rain");
      driver.findElement(By.name("selfish")).click();
      driver.findElement(By.xpath("//button[contains(text(), 'Submit')]")).click();
      System.out.println("water - rain created");
      Thread.sleep(4000);
      driver.navigate().back();
      // water - splash
      driver.findElement(By.id("name")).sendKeys("Brain Wils");
      driver.findElement(By.id("birthDate")).sendKeys("10/30/1970");
      jse = (JavascriptExecutor) driver;
      jse.executeScript("scroll(0, 800)");
      driver.findElement(By.id("affinity")).sendKeys("water");
      driver.findElement(By.id("power")).sendKeys("splash");
      driver.findElement(By.name("selfish")).click();
      driver.findElement(By.xpath("//button[contains(text(), 'Submit')]")).click();
      System.out.println("water - splash created");
      Thread.sleep(4000);
      driver.navigate().back();
      // earth - quake
      driver.findElement(By.id("name")).sendKeys("Breed Mcwash");
      driver.findElement(By.id("birthDate")).sendKeys("10/30/1970");
      jse = (JavascriptExecutor) driver;
      jse.executeScript("scroll(0, 800)");
      driver.findElement(By.id("affinity")).sendKeys("earth");
      driver.findElement(By.id("power")).sendKeys("Earth quake");
      driver.findElement(By.name("selfish")).click();
      driver.findElement(By.xpath("//button[contains(text(), 'Submit')]")).click();
      System.out.println("earth - quake");
      Thread.sleep(4000);
      driver.navigate().back();
      // earth - stone
      driver.findElement(By.id("name")).sendKeys("Walker Tika");
      driver.findElement(By.id("birthDate")).sendKeys("10/30/1970");
      jse = (JavascriptExecutor) driver;
      jse.executeScript("scroll(0, 800)");
      driver.findElement(By.id("affinity")).sendKeys("earth");
      driver.findElement(By.id("power")).sendKeys("stone");
      driver.findElement(By.name("selfish")).click();
      driver.findElement(By.xpath("//button[contains(text(), 'Submit')]")).click();
      System.out.println("earth - stone");
      Thread.sleep(4000);
      driver.navigate().back();
      // air - wind, it is going to fail cause the air options are not
      // available
      driver.findElement(By.id("name")).sendKeys("Tikuo Jol");
      driver.findElement(By.id("birthDate")).sendKeys("10/30/1970");
      jse = (JavascriptExecutor) driver;
      jse.executeScript("scroll(0, 800)");
      driver.findElement(By.id("affinity")).sendKeys("air");
      driver.findElement(By.id("power")).sendKeys("wind");
      driver.findElement(By.name("selfish")).click();
      driver.findElement(By.xpath("//button[contains(text(), 'Submit')]")).click();
      
      Thread.sleep(4000);
      if (ElemEmpty) {
        System.out.println("air - wind failed");
        driver.get("http://qa-case-study.hydrane.com");
      }else{System.out.println("air - wind");
      }
      // air - fly, it is going to fail cause the air options are not
      // available
      driver.findElement(By.id("name")).sendKeys("Hushan Xumar");
      driver.findElement(By.id("birthDate")).sendKeys("10/30/1970");
      jse = (JavascriptExecutor) driver;
      jse.executeScript("scroll(0, 800)");
      driver.findElement(By.id("affinity")).sendKeys("air");
      driver.findElement(By.id("power")).sendKeys("fly");
      driver.findElement(By.name("selfish")).click();
      driver.findElement(By.xpath("//button[contains(text(), 'Submit')]")).click();
      
      Thread.sleep(4000);
      if (ElemEmpty) {
        System.out.println("air - fly failed");
        driver.get("http://qa-case-study.hydrane.com");
      }else{
        System.out.println("air - fly");
      }
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

  }
}

