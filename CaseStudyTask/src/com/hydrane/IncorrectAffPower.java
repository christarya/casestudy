package com.hydrane;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

public class IncorrectAffPower {

  private static final boolean ElemEmpty = true;
  public WebDriver driver;
  JavascriptExecutor jse;

  public void incorrect() {
    try {
      // fire - meteor, spear
      driver.findElement(By.id("name")).sendKeys("Kristine Mukuchyan");
      driver.findElement(By.id("birthDate")).sendKeys("11/24/1991");
      jse = (JavascriptExecutor) driver;
      jse.executeScript("scroll(0, 800)");
      driver.findElement(By.id("affinity")).sendKeys("fire");
      Thread.sleep(4000);
      driver.findElement(By.id("power"));
      sendKeys("splash");
      sendKeys("rain");
      sendKeys("stone");
      sendKeys("Earth quake");
      sendKeys("wind");
      sendKeys("fly");
      if (sendKeys("meteor") == true) {
        driver.findElement(By.name("selfish")).click();
        driver.findElement(By.xpath("//button[contains(text(), 'Submit')]")).click();
      } else {
        System.out.println("Incorrect Power - Fire");
      }
      driver.navigate().refresh();
      Thread.sleep(4000);
      if (ElemEmpty) {
        driver.get("http://qa-case-study.hydrane.com");
      }

      // water - rain, splash
      driver.findElement(By.id("name")).sendKeys("Karen Kalpakchyan");
      driver.findElement(By.id("birthDate")).sendKeys("08/30/1989");
      jse = (JavascriptExecutor) driver;
      jse.executeScript("scroll(0, 800)");
      driver.findElement(By.id("affinity")).sendKeys("water");
      Thread.sleep(4000);
      driver.findElement(By.id("power"));
      sendKeys("meteor");
      sendKeys("spear");
      sendKeys("stone");
      sendKeys("Earth quake");
      sendKeys("wind");
      sendKeys("fly");

      if (sendKeys("rain") == true) {
        driver.findElement(By.name("selfish")).click();
        driver.findElement(By.xpath("//button[contains(text(), 'Submit')]")).click();
      } else {
        System.out.println("Incorrect Power - Water");
      }
      driver.navigate().refresh();
      Thread.sleep(4000);
      if (ElemEmpty) {
        driver.get("http://qa-case-study.hydrane.com");
      }

      // earth - Earth quake, stone
      driver.findElement(By.id("name")).sendKeys("K&K");
      driver.findElement(By.id("birthDate")).sendKeys("08/30/1989");
      jse = (JavascriptExecutor) driver;
      jse.executeScript("scroll(0, 800)");
      driver.findElement(By.id("affinity")).sendKeys("earth");
      Thread.sleep(4000);
      driver.findElement(By.id("power"));
      sendKeys("meteor");
      sendKeys("spear");
      sendKeys("rain");
      sendKeys("splash");
      sendKeys("wind");
      sendKeys("fly");

      if (sendKeys("stone") == true) {
        driver.findElement(By.name("selfish")).click();
        driver.findElement(By.xpath("//button[contains(text(), 'Submit')]")).click();
      } else {
        System.out.println("Incorrect Power - Earth");
      }
      driver.navigate().refresh();
      Thread.sleep(4000);
      if (ElemEmpty) {
        driver.get("http://qa-case-study.hydrane.com");
      }

      // air - wind, fly
      driver.findElement(By.id("name")).sendKeys("K&K");
      driver.findElement(By.id("birthDate")).sendKeys("08/30/1989");
      jse = (JavascriptExecutor) driver;
      jse.executeScript("scroll(0, 800)");
      driver.findElement(By.id("affinity")).sendKeys("air");
      Thread.sleep(4000);
      driver.findElement(By.id("power"));
      sendKeys("meteor");
      sendKeys("spear");
      sendKeys("rain");
      sendKeys("splash");
      sendKeys("Earth quake");
      sendKeys("stone");

      if (sendKeys("wind") == true) {
        driver.findElement(By.name("selfish")).click();
        driver.findElement(By.xpath("//button[contains(text(), 'Submit')]")).click();
      } else {
        System.out.println("Incorrect Power - Air");
      }
      driver.navigate().refresh();
      Thread.sleep(4000);
      if (ElemEmpty) {
        driver.get("http://qa-case-study.hydrane.com");
      }
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

  }

  private boolean sendKeys(String string) {

    return false;
  }
}
