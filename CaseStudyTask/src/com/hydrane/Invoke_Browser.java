package com.hydrane;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Invoke_Browser {

  public WebDriver driver;
  JavascriptExecutor jse;

  public void invokeBrowser() {

    try {
      System.setProperty("webdriver.chrome.driver",
          "C:\\Programing\\Test Automation\\Selenium\\chromedriver\\chromedriver.exe");
      driver = new ChromeDriver();
      driver.manage().window().maximize();
      driver.manage().deleteAllCookies();
      driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
      driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
      driver.get("http://qa-case-study.hydrane.com");
      System.out.println("Breowser opened successful - Invoke_BrowserClass");
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
