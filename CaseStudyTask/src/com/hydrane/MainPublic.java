package com.hydrane;

public class MainPublic {

	  public static void main(String[] args) {

	    Invoke_Browser myObj = new Invoke_Browser();
	    myObj.invokeBrowser();
	    Affinity_Power myObj1 = new Affinity_Power();
	    myObj1.driver = myObj.driver;
	    myObj1.correct();
	    Members myObj2 = new Members();
	    myObj2.driver = myObj.driver;
	    myObj2.navigateMembers();
	    UncheckedCheckbox myObj3 = new UncheckedCheckbox();
	    myObj3.driver = myObj.driver;
	    myObj3.submit();
	    WrongBirthdayData myObj4 = new WrongBirthdayData();
	    myObj4.driver = myObj.driver;
	    myObj4.birthday();
	    IncorrectAffPower myObj5 = new IncorrectAffPower();
	    myObj5.driver = myObj.driver;
	    myObj5.incorrect();
	    CloseQuiteBbrowser myObj6 = new CloseQuiteBbrowser();
	    myObj6.driver = myObj.driver;
	    myObj6.closeBrowser();

	  }
	}
