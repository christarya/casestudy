package com.hydrane;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

public class Members {
  private static final boolean ElemEmpty = true;
  public WebDriver driver;
  JavascriptExecutor jse;

  public void navigateMembers() {
    try {
      driver.findElement(By.xpath("//div[@id='navbarSupportedContent']/ul/li[2]")).click();
      System.out.println("Members page opened");
      Thread.sleep(4000);
      driver.findElement(By.className("navbar-brand")).click();
      System.out.println("Villain Quarter opened");
      Thread.sleep(4000);
      driver.navigate().back();
      if (ElemEmpty) {
        System.out.println("404 page not found");
        driver.get("http://qa-case-study.hydrane.com");
      } else {
        System.out.println("navgiagted back to members page");
      }
      Thread.sleep(4000);
      driver.navigate().refresh();
      if (ElemEmpty) {
        System.out.println("404 page not found");
        driver.get("http://qa-case-study.hydrane.com");
      } else {
        System.out.println("page refresdhed success");
      }

      Thread.sleep(4000);

    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

}

