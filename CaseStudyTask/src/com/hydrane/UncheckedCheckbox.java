package com.hydrane;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

public class UncheckedCheckbox {

  private static final boolean ElemEmpty = true;
  public WebDriver driver;
  JavascriptExecutor jse;

  public void submit() {

    try {
      driver.findElement(By.id("name")).sendKeys("Kristine Mukuchyan");
      driver.findElement(By.id("birthDate")).sendKeys("11/24/1991");
      jse = (JavascriptExecutor) driver;
      jse.executeScript("scroll(0, 800)");
      driver.findElement(By.id("affinity")).sendKeys("fire");
      driver.findElement(By.id("power")).sendKeys("meteor");
      // driver.findElement(By.name("selfish")).click();
      driver.findElement(By.xpath("//button[contains(text(), 'Submit')]")).click();
      System.out.println("checkbox is not checked");
      Thread.sleep(4000);
      driver.navigate().back();
      if (ElemEmpty) {
        driver.get("http://qa-case-study.hydrane.com");
      }
    } catch (InterruptedException e) {

      e.printStackTrace();
    }
  }
}
